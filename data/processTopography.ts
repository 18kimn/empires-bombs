import { promises as fs } from 'fs'
import type { FeatureCollection } from 'geojson'
import { dirname, resolve } from 'path'
import { fileURLToPath } from 'url'
import groupFeatures from './groupFeatures'
import { mergeGroup, simplifyTopology, stringifyTopology } from './helpers'

const __dirname = dirname(fileURLToPath(import.meta.url))

async function processTopography() {
  const shapes = (await fs
    .readFile(resolve(__dirname, 'raw/topography.json'), 'utf-8')
    .then((text) => JSON.parse(text))) as FeatureCollection
  const { features } = shapes
  const level = ['elevation']

  const groupedFeatures = groupFeatures(features, level)
  const featureString = Object.values(groupedFeatures)
    .map((groupedFeatureSet) => {
      const { meta, features } = groupedFeatureSet
      const mergedTopo = mergeGroup(features)
      return stringifyTopology(mergedTopo, meta, null)
    })
    .join('\n')

  fs.writeFile(
    resolve(__dirname, '../public', 'topography.json'),
    featureString
  )
}

processTopography()
