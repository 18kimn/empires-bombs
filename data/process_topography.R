process_topography <- function(){
  box::use(sf[
    read_sf, write_sf, st_as_sf,
    st_make_valid, st_area,
    st_cast, st_combine, st_union, sf_use_s2
  ])
  box::use(terra[rast, focal, as.polygons, crop])
  box::use(dplyr[filter, group_by, summarize, mutate, group_split, `%>%`])
  box::use(tibble[tibble])
  box::use(purrr[map_dbl, map_dfr])
  box::use(rmapshaper[ms_simplify])
  box::use(smoothr[smooth, drop_crumbs])
  box::use(units[set_units])
  sf_use_s2(FALSE)
  # raster data isn't supposed to be treated as GeoJSON or vector data in general
  # since it represents pixels, of which there are millions, and are computationally expensive
  # I don't care though :) so this script takes a raster elevation file, buckets it, and exports
  # it as GeoJSON for use with d3

  crop_area <- rast(
    xmin = 120, xmax = 135,
    ymin = 27,  ymax = 47
  )

  asia <- rast("data/raw/ETOPO1_Ice_g_geotiff.tif") %>%
    crop(crop_area) %>%
    focal(fun = "mean") %>%
    as.polygons() %>%
    st_as_sf() %>%
    filter(focal_mean >= 0)

  # The above creates one row for every unit of elevation (1m, 2m, etc)
  # We're going to cut down on detail by aggregating into groups
  # So 37m is placed in the 0 bucket, 150m is placed in the 100 bucket, etc.
  buckets <- c(0, 100, 250, 500, 750)
  pick_bucket <- function(num) {
    if (is.na(num))
      return(num)
    logicals <- num < buckets
    if (all(!logicals))
      return(buckets[length(buckets)])
    buckets[which.max(logicals) - 1]
  }

  asia_bucketed <- asia %>%
    mutate(elevation = map_dbl(focal_mean, pick_bucket)) %>%
    filter(elevation != 0) %>%
    group_by(elevation) %>%
    summarize() #takes a really long time...


  # a helper to filter out polygons that are below a certain size and just show up as tiny polygons
  # also helps reduce file size by quite a bit
  filter_geometries <- function(group_dta) {
    polygons <- st_cast(group_dta$geometry, "POLYGON")
    polygons <- polygons[as.numeric(st_area(st_make_valid(polygons))) > 5e+07]
    tibble(elevation = group_dta$elevation, geometry = polygons)
  }

  asia_filtered <- asia_bucketed %>%
    group_split(elevation) %>%
    map_dfr(filter_geometries) %>%
    st_as_sf() %>%
    group_by(elevation) %>%
    summarize(geometry = st_make_valid(geometry) %>%
                st_union()) %>%
    drop_crumbs(threshold = set_units(10, km^2)) %>%
    smooth(method = "ksmooth", smoothness = 3) %>%
    ms_simplify(keep = 0.01)
  # smooth() adds points to do its thing, so we need a pretty aggressive simplifier

  write_sf(asia_filtered, "data/raw/topography.json", driver = "GeoJSON", delete_dsn = TRUE)
}

process_topography()
