import {data} from './store'

export const palette = {
  black: '#000000',
  gray: '#c3c3c3',
  red: '#660000',
  green: '#55d175',
  yellow: '#B58900',
  blue: '#72BFDC',
  purple: '#5C21A5',
  cyan: '#158C86',
  white: '#FFFFFF',
  background: '#ACD0A5',
  light_background: '#D8D5C7',
  terrain: {
    100: '#BDCC96',
    250: '#E1E4B5',
    500: '#CAB982',
    750: '#AA8753'
  }
}

export const dayLength = 1000

/** small wrappers for ms -> date and back again */
export function msToDate(milliseconds: number) {
  const start = new Date('1951-05-31')
  start.setDate(
    start.getDate() + Math.floor(milliseconds / dayLength)
  )
  return start
}

/** date can be any date-interpretable string */
export function dateToMs(date: Date | string) {
  const start = new Date('1951-05-31')
  const newDate = new Date(date) // to avoid mutating the original
  const diff = (newDate.getTime() - start.getTime()) / (3600 * 24)
  return diff
}

/** another small wrapper, used to always have the correct zoom on a
 * canvas */
export function useTransform(context: CanvasRenderingContext2D) {
  if (data.transform) {
    const {x, y, k} = data.transform
    context.translate(x, y)
    context.scale(k, k)
  }
}
