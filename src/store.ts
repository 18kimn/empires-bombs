import type {Border, Bomb, Topograph} from './types'
import {writable} from 'svelte/store'
import type {ZoomTransform} from 'd3-zoom'
import type {GeoProjection} from 'd3-geo'
export const data = {
  borders: {
    type: 'FeatureCollection' as 'FeatureCollection',
    features: [] as Border[]
  },
  topography: [] as Topograph[],
  bombs: [] as Bomb[],
  projection: undefined as GeoProjection,
  tons: 0,
  transform: undefined as ZoomTransform,
  forceRedraw: false
}

export const actions = {
  startBombs: undefined,
  stopBombs: undefined,
  clearBombs: undefined,
  clearCanvas: undefined,
  animate: undefined
}

export const state = writable({
  day: undefined,
  tons: 0
})
