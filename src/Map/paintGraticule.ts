import type {Feature} from 'geojson'
import {geoPath, geoGraticule} from 'd3-geo'
import {data} from '../store'
import {useTransform} from '../utils'

const graticule = {
  type: 'Feature',
  properties: {},
  geometry: geoGraticule().stepMinor([3, 3])()
} as Feature

let gratPath: Path2D

/** graticules are the longitude-latitude grid in the background*/
function paintGraticule(context: CanvasRenderingContext2D) {
  if (data.forceRedraw) {
    context.clearRect(0, 0, window.innerWidth, window.innerHeight)
  }
  context.save()
  context.globalAlpha = 0.5
  context.lineWidth = 0.5
  context.strokeStyle = '#7c7c7c'
  useTransform(context)
  if (typeof gratPath === 'undefined') {
    const gratPath2D = new Path2D() //@ts-ignore
    const gratGeoPath = geoPath(data.projection).context(gratPath2D)
    gratGeoPath(graticule)
    graticule.properties.shouldStroke = true
    gratPath = gratPath2D
  }

  if (graticule.properties.shouldStroke || data.forceRedraw) {
    context.beginPath()
    context.stroke(gratPath)
    graticule.properties.shouldStroke = false
  }
  context.restore()
}

export default paintGraticule
