import type {Topograph} from '../types'
import {palette, useTransform} from '../utils'
import {data} from '../store'

const {terrain} = palette

/** essentially the background */
function paintTopography(
  context: CanvasRenderingContext2D,
  time: number,
  topography: Topograph[],
  dims: [number, number]
) {
  if (!context) return
  context.save()
  if (data.forceRedraw) {
    context.clearRect(0, 0, dims[0], dims[1])
  }

  useTransform(context)

  topography.forEach((topograph, index) => {
    if (typeof topograph.properties.startTime === 'undefined') {
      topograph.properties.startTime = 3 * index + time
    }
    const {startTime, elevation} = topograph.properties

    if (startTime >= time && !data.forceRedraw) return

    const color = terrain[elevation]
    const alpha = Math.min(1, (time - startTime) / 400)
    if (alpha === 1 && !data.forceRedraw) return

    context.globalAlpha = alpha
    context.fillStyle = color
    context.beginPath()
    context.fill(topograph.properties.path2d)
  })

  context.restore()
}

export default paintTopography
