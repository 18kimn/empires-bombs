import type {Bomb} from '../types'
import {
  palette,
  dayLength,
  msToDate,
  useTransform,
  dateToMs
} from '../utils'
import {data} from '../store'

/** */
function paintBombs(
  contexts: CanvasRenderingContext2D[],
  time: number,
  bombs: Bomb[]
) {
  const [animatedContext, longTermContext] = contexts
  animatedContext.clearRect(
    0,
    0,
    window.innerWidth,
    window.innerHeight
  )
  if (data.forceRedraw) {
    longTermContext.clearRect(
      0,
      0,
      window.innerWidth,
      window.innerHeight
    )
  }
  contexts.map((context) => {
    context.save()
    context.fillStyle = palette.red
    useTransform(context)
  })
  const now = msToDate(time)
  let currentTons = 0
  bombs.forEach((bomb) => {
    const bombingDate = new Date(bomb.date)
    if (bombingDate > now) return
    if (bomb.lon === 'NA' || bomb.lat === 'NA') return
    if (!bomb.startTime) {
      const noise = Math.random() * 500
      bomb.startTime = dateToMs(bombingDate) + noise
    }
    if (bomb.startTime > time) return
    if (bomb.finished && !data.forceRedraw) return

    currentTons += bomb.tons || 0
    const [x, y] = data.projection([bomb.lon, bomb.lat])
    const maxRadius = 8 * Math.log(bomb.tons + 1)
    const minRadius = 4 * Math.log(bomb.tons + 1)
    const normTime = (time - bomb.startTime) / dayLength
    if (time >= bomb.startTime + dayLength) {
      bomb.finished = true
      longTermContext.beginPath()
      longTermContext.globalAlpha = 0.2
      longTermContext.arc(x, y, minRadius, 0, Math.PI * 2)
      return
    }
    animatedContext.beginPath()
    animatedContext.arc(
      x,
      y,
      Math.max(maxRadius - maxRadius * normTime, minRadius),
      0,
      Math.PI * 2
    )
    animatedContext.globalAlpha = 1 - 0.8 * normTime
    animatedContext.fill()
  })

  /** trying to batch calls to context.fill()
   * this actually makes some circles strangely disappear ??
   * but I can't figure out why and we need better perf */
  longTermContext.fill()

  contexts.map((context) => context.restore())
  data.tons = currentTons
}

export default paintBombs
