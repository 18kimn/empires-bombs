import {geoOrthographic} from 'd3-geo'
import type {FeatureCollection} from 'geojson'
import type {Borders, Topograph} from 'src/types'
import paintBorders from './paintBorders'
import paintTopography from './paintTopography'
import paintGraticule from './paintGraticule'
import {data} from '../store'

/** tiny wrapper for the actual paint functions */
export function drawLand(
  contexts: CanvasRenderingContext2D[],
  time: number,
  shapes: { topography: Topograph[]; borders: Borders },
  dims: [number, number]
) {
  const {topography, borders} = shapes

  paintGraticule(contexts[0], dims)
  paintBorders(contexts[1], time, borders, dims)
  paintTopography(contexts[2], time, topography, dims)
}

const bounds = {
  type: 'LineString',
  coordinates: [
    [130, 43],
    [125, 33]
  ]
}

data.projection = geoOrthographic().rotate([-120, -30, -8])

/** just mutates a single object instead of creating
 * a new one, so references to the old are updated automatically
 * don't yell at me about mutability
 * */
export function updateProjection(width: number, height: number) {
  data.projection.fitExtent(
    [
      [0, 0],
      [width, height]
    ],
    bounds as any as FeatureCollection
  )
}
