import {data} from '../store'
import type {Borders} from '../types'
import {palette, useTransform} from '../utils'

/** I draw borders separately from topography
 * so that even if the topography fails to load, the (smaller, simpler)
 * border shapes can still appear
 * */
function paintBorders(
  context: CanvasRenderingContext2D,
  time: number,
  borders: Borders
) {
  if (!context) return
  context.save()
  if (data.forceRedraw) {
    context.clearRect(0, 0, window.innerWidth, window.innerHeight)
  }
  useTransform(context)
  context.strokeStyle = '#7c7c7c'
  context.lineWidth = 1
  context.fillStyle = palette.background
  borders.features.forEach((border, index) => {
    if (typeof border.properties.startTime === 'undefined') {
      border.properties.startTime = 3 * index + time
    }
    if (border.properties.startTime >= time) return
    const alpha = Math.min(
      1,
      (time - border.properties.startTime) / 400
    )
    if (alpha === 1 && !data.forceRedraw) return
    // if the fade-in is complete don't waste time by rerendering
    context.globalAlpha = alpha

    context.beginPath()
    context.fill(border.properties.path2d)
    context.stroke(border.properties.path2d)
  })
  context.restore()
}

export default paintBorders
