import type { Feature, FeatureCollection } from 'geojson'

export interface Bomb {
  date: Date
  mission_number: number
  tons: number
  target_name: string
  type: string
  lat: number | 'NA'
  lon: number | 'NA'
  finished: boolean
  startTime: number
  direction: 'growing' | 'fading'
}

// conceptually a single element of the
// topography of East Asia
export interface Topograph extends Feature {
  properties: {
    elevation: number
    startTime: number
    path2d: Path2D
  }
}

export interface Border extends Feature {
  properties: {
    startTime: number
    path2d: Path2D
  }
}

export interface Borders extends FeatureCollection {
  features: Border[]
}
