- make a low-res/low-bandwidth version of the map
- make the site work on mobile (the styling gets messed up)
- make the map respond to resizes, like if the user expands the
  browser the map shouldn't appear squished
- report some more things from the map -- like what sites there were,
  the size of each bomb, what it was meant to target in the "explore"
  section
- write up text for each section (this is probably the largest task)
- hopefully pick a better looking font, but if we don't it's ok
- hopefully pick a better title, but if we don't it's ok

KNOWN BUGS

- when you click on "begin", then "back," then "explore," "explore"
  doesn't start from
